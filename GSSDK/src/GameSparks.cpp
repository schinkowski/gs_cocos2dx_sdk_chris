#include "GameSparks.h"
#include "gsObject.h"
#include "json.h"


#include <assert.h>

#define _CRT_SECURE_NO_DEPRECATE 

//TODO: Need to move this into the lower level SDK to break dependency
#include "easywsclient.hpp"

namespace {
	static bool _gsInit = false;
	static bool _gsInitOutcome = false;	

//
//	Code below came from: http://stackoverflow.com/questions/2915672/snprintf-and-visual-studio-2010
//
#ifdef _MSC_VER
#include <stdarg.h>
#define snprintf c99_snprintf

inline int c99_vsnprintf(char* str, size_t size, const char* format, va_list ap)
{
	int count = -1;

	if (size != 0)
		count = _vsnprintf_s(str, size, _TRUNCATE, format, ap);
	if (count == -1)
		count = _vscprintf(format, ap);

	return count;
}

inline int c99_snprintf(char* str, size_t size, const char* format, ...)
{
	int count;
	va_list ap;

	va_start(ap, format);
	count = c99_vsnprintf(str, size, format, ap);
	va_end(ap);

	return count;
}
#endif
};
//	End of Stack overflow code

using namespace GameSparks;

namespace GameSparks
{


	//
	//	ctor/dtor
	//
	CGamesparks::CGamesparks(gamesparks::GSObject *_gsObject)
		: m_gsObject(_gsObject)
		, m_Status(Status::keDisconnected)

		, m_CurrentRequestGen(0)
		, m_RequestGenData(0)
		, m_CurrentRequestID(0)
		
		, m_OnConnectCB(0)
		, m_OnDisconnectCB(0)
		, m_OnErrorCB(0)
		, m_OnMessageCB(0)

		, m_OnConnectData(0)
		, m_OnDisconnectData(0)
		, m_OnErrorData(0)
		, m_OnMessageData(0)
		
	{}
	CGamesparks::~CGamesparks()
	{
		//Should clean everything up
		delete m_gsObject;
	}

	//
	//	Public Interface methods
	//

	CGamesparks::Status::Enum CGamesparks::getStatus()
	{
		return m_Status;
	}

	void CGamesparks::close()
	{
		m_gsObject->close();
	}

	bool CGamesparks::suspend()
	{
		//TODO This will close the websocket but maintain the auth ID
		return m_gsObject->suspend();
	}

	bool CGamesparks::resume()
	{
		//TODO This will reconnect and try to auth with the auth ID
        
        connect( apiKey.c_str(), appSecret.c_str(), targetEndPoint );
		return m_gsObject->resume();
	}

	bool CGamesparks::connect(const char*  target, const char* secret, TargetEndpoint::Enum endpoint)
	{
		std::string endPoint = (endpoint == TargetEndpoint::kePreview)
			? std::string("wss://preview.gamesparks.net/websockets/") + std::string(target)
			: std::string("wss://service.gamesparks.net/websockets/") + std::string(target);

        apiKey = target;
        appSecret = secret;
        targetEndPoint = endpoint;
        
		if (m_gsObject->connect(endPoint, secret))
		{
			m_Status = Status::keConnecting;
			return true;
		}
        
        Json::Value data;
        data[ "error" ] = "CANNOT_CONNECT";
        m_OnError( data );
        
		return false;
	}

	bool CGamesparks::isAuthenticated()
	{
		return m_gsObject->isAuthenticated();
	}

	void CGamesparks::setStoreCallback(GSStoreValueCallback cb, void *userData)
	{
		m_gsObject->setStoreCallback(cb, userData);
	}
	void CGamesparks::setRetrieveCallback(GSRetrieveValueCallback cb, void *userData)
	{
		m_gsObject->setRetrieveCallback(cb, userData);
	}


	void CGamesparks::update(int elapsedTimeMS)
	{
		m_gsObject->update( elapsedTimeMS );
		std::vector<std::string> deadList;
        
		//Update all of the timeout values and create a list of all requests
		//that timeout on this update.
		for ( requestList::iterator cRequest = m_PendingRequests.begin();
             cRequest != m_PendingRequests.end();
             ++cRequest )
		{
			if ( cRequest->second.timeout <= (unsigned int)elapsedTimeMS )
			{
				//We timeout on this update
				deadList.push_back(cRequest->first);
			}
			else
			{
				cRequest->second.timeout -= elapsedTimeMS;
			}
		}
		
		Json::Value timeoutError;
		timeoutError["error"] = "timeout";
        
		//Remove timed out requests
		for ( std::vector<std::string>::iterator cDeadRequest = deadList.begin();
             cDeadRequest != deadList.end();
             ++cDeadRequest )
		{
			//Call the callback with an error
			gsRequest &cRequest = m_PendingRequests[*cDeadRequest];
			
			timeoutError["requestID"] = cRequest.requestID;
			cRequest.callback(timeoutError, cRequest.data);
            
			m_PendingRequests.erase (*cDeadRequest);
		}
	}

	void CGamesparks::setRequestIDGen(GSRequestIDGen nGenerator, void *data)
	{
		m_CurrentRequestGen = nGenerator;
		m_RequestGenData = data;
	}

	std::string CGamesparks::m_GenRequest(void *data)
	{
		CGamesparks *self = (CGamesparks*)data;
		unsigned long long cID = self->m_CurrentRequestID++;

		char strID[32];
		snprintf(strID, sizeof(strID), "%llu", cID);
		
		return strID;
	}

	bool CGamesparks::sendMessage(const char*  message, GSMessageCB callback /*= 0*/, unsigned int timeout /*= 0*/, void *userData)
	{
        Json::Value dummy;
		return sendMessageWithData(message, dummy, callback, timeout, userData);
	}

	bool CGamesparks::sendMessageWithData(const char*  message, Json::Value& data, GSMessageCB callback /*= 0*/, unsigned int timeout /*= 0*/, void *userData)
	{
		std::string requestID = (m_CurrentRequestGen == 0)
        ? m_GenRequest(this)
        : m_CurrentRequestGen(m_RequestGenData);
        
		Json::Value localData (data);	//Take a copy of the data
		localData["@class"] = (message[0] == '.')
        ? message
        : std::string(".") + message;
		
		localData["requestId"] = requestID;
        
		bool result = m_gsObject->sendJsonMessage(localData);
        
		//	Only insert this request if sending it was successful
		if ( result && callback != 0 && timeout != 0 )
		{
			//Ensure we are not re-using a requestID
			assert (m_PendingRequests.find(requestID) == m_PendingRequests.end());
			
			gsRequest nRequest =
			{
				requestID,
				callback,
				timeout,
				userData
			};
            
			m_PendingRequests[requestID] = nRequest;
		}
        
		return result;
	}
    
    void CGamesparks::setKeepAliveInterval( float _interval )
    {
        m_gsObject->setKeepAliveInterval( _interval );
    }
    
	void CGamesparks::setPlatform(const char*  platform, const char*  os)
    {
        m_gsObject->setPlatform( platform, os );
    }
    
	//
	//	Member callback setters
	//
	void CGamesparks::setOnError(GSMessageCB callback, void *data)
	{
		m_OnErrorCB = callback;
		m_OnErrorData = data;
	}

	void CGamesparks::setOnMessage(GSMessageCB callback, void *data)
	{
		m_OnMessageCB = callback;
		m_OnMessageData = data;
	}

	void CGamesparks::setOnConnect(GSConnectionCB callback, void *data)
	{
		m_OnConnectCB = callback;
		m_OnConnectData = data;
	}

	void CGamesparks::setOnDisconnect(GSConnectionCB callback, void *data)
	{
		m_OnDisconnectCB = callback;
		m_OnDisconnectData = data;
	}

	void CGamesparks::setLogCallback (GSLogCallback cb, void *userData)
	{
		m_gsObject->setLogCallback(cb, userData);
	}

	//
	//	Member callback handlers
	//
	void CGamesparks::m_OnConnect(Json::Value& data)
	{
		m_Status = Status::keConnected;
		if ( m_OnConnectCB ) m_OnConnectCB(m_OnConnectData);
	}

	void CGamesparks::m_OnMessage(Json::Value& data)
	{
		Json::FastWriter writer;
        std::string str = writer.write( data );
        
		//TODO: This is currently set to passthrough until we implement
		//the callback mapping.
		requestList::iterator sigHandler = m_PendingRequests.find(data["requestId"].asString());
        
		if ( sigHandler != m_PendingRequests.end() )
		{
			//We have a handler for this request
			gsRequest &cRequest = sigHandler->second;
            
			//Issue callback
			cRequest.callback(data, cRequest.data);
            
			//Remove request from pending container
			m_PendingRequests.erase(sigHandler);
		}
		else
		{
			//No handler, lets call generic message handler
			if ( m_OnMessageCB ) m_OnMessageCB(data, m_OnMessageData);
		}
	}

	void CGamesparks::m_OnError(Json::Value&  data)
	{
		//Call user error handler... not sure what else to be done
		if ( m_OnErrorCB ) m_OnErrorCB(data, m_OnErrorData);
	}

	void CGamesparks::m_OnDisconnect(Json::Value&  data)
	{
		m_Status = Status::keDisconnected;
		if ( m_OnDisconnectCB ) m_OnDisconnectCB(m_OnDisconnectData);

		m_PendingRequests.clear();
	}

	//
	//	Static callback handlers
	//
	void CGamesparks::cb_connectCallback(const gamesparks::CallbackData & response, void *self)
	{
		((CGamesparks*)self)->m_OnConnect(response.json);
	}

	void CGamesparks::cb_messageCallback(const gamesparks::CallbackData & response, void *self)
	{
		((CGamesparks*)self)->m_OnMessage(response.json);
	}

	void CGamesparks::cb_errorCallback(const gamesparks::CallbackData & response, void *self)
	{
		((CGamesparks*)self)->m_OnError(response.json);
	}

	void CGamesparks::cb_disconnectCallback(const gamesparks::CallbackData & response, void *self)
	{
		((CGamesparks*)self)->m_OnDisconnect(response.json);
	}

	//
	//	Static member methods
	//
	IGameSparks * CGamesparks::createConcrete()
	{
		if ( !_gsInitOutcome ) return 0;

		gamesparks::GSObject *newGS = new gamesparks::GSObject();
		if ( newGS == 0 ) return 0;

		CGamesparks *newGamesparks = new CGamesparks(newGS);

		//Set the callbacks here... if anything goes wrong we can still bail
		newGS->setCallback(gamesparks::GSObject::CallbackType::keOnConnect, 
			CGamesparks::cb_connectCallback, 
			newGamesparks);
		newGS->setCallback(gamesparks::GSObject::CallbackType::keOnMessage, 
			CGamesparks::cb_messageCallback, 
			newGamesparks);
		newGS->setCallback(gamesparks::GSObject::CallbackType::keOnError, 
			CGamesparks::cb_errorCallback, 
			newGamesparks);
		newGS->setCallback(gamesparks::GSObject::CallbackType::keOnDisconnect, 
			CGamesparks::cb_disconnectCallback, 
			newGamesparks);

		//All ready to return
		return newGamesparks;
	}

	IGameSparks * IGameSparks::Create()
	{
		return CGamesparks::createConcrete();
	}

	void IGameSparks::Cleanup()
	{
		if (_gsInitOutcome)
		{
			easywsclient::cleanupEasyWSClient();
		}
	}

	bool IGameSparks::Init()
	{
		if (!_gsInit) 
		{
			_gsInit = true;
			_gsInitOutcome = easywsclient::initEasyWSClient();
		}

		return _gsInitOutcome;
	}
}

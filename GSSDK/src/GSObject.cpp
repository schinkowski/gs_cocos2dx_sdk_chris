#include "GSObject.h"
#include "easywsclient.hpp"
#include <openssl/hmac.h>

#include "json.h"

using easywsclient::WebSocket;
using namespace Json;

namespace 
{

	/*
	base64.cpp and base64.h

	Copyright (C) 2004-2008 Ren� Nyffenegger

	This source code is provided 'as-is', without any express or implied
	warranty. In no event will the author be held liable for any damages
	arising from the use of this software.

	Permission is granted to anyone to use this software for any purpose,
	including commercial applications, and to alter it and redistribute it
	freely, subject to the following restrictions:

	1. The origin of this source code must not be misrepresented; you must not
	claim that you wrote the original source code. If you use this source code
	in a product, an acknowledgment in the product documentation would be
	appreciated but is not required.

	2. Altered source versions must be plainly marked as such, and must not be
	misrepresented as being the original source code.

	3. This notice may not be removed or altered from any source distribution.

	Ren� Nyffenegger rene.nyffenegger@adp-gmbh.ch

	*/

//#include "base64.h"
//#include <iostream>
	#include <ctype.h> 

	static const std::string base64_chars =
    "ABCDEFGHIJKLMNOPQRSTUVWXYZ"
    "abcdefghijklmnopqrstuvwxyz"
    "0123456789+/";
    
    
	static inline bool is_base64(unsigned char c)
	{
		return (isalnum(c) || (c == '+') || (c == '/'));
	}
    
	std::string base64_encode(unsigned char const* bytes_to_encode, unsigned int in_len)
	{
		std::string ret;
		int i = 0;
		int j = 0;
		unsigned char char_array_3[3];
		unsigned char char_array_4[4];
        
		while (in_len--)
		{
			char_array_3[i++] = *(bytes_to_encode++);
			if (i == 3)
			{
				char_array_4[0] = (char_array_3[0] & 0xfc) >> 2;
				char_array_4[1] = ((char_array_3[0] & 0x03) << 4) + ((char_array_3[1] & 0xf0) >> 4);
				char_array_4[2] = ((char_array_3[1] & 0x0f) << 2) + ((char_array_3[2] & 0xc0) >> 6);
				char_array_4[3] = char_array_3[2] & 0x3f;
                
				for (i = 0; (i <4); i++)
					ret += base64_chars[char_array_4[i]];
                i = 0;
			}
		}
        
		if (i)
		{
			for (j = i; j < 3; j++)
                char_array_3[j] = '\0';
            
			char_array_4[0] = (char_array_3[0] & 0xfc) >> 2;
			char_array_4[1] = ((char_array_3[0] & 0x03) << 4) + ((char_array_3[1] & 0xf0) >> 4);
			char_array_4[2] = ((char_array_3[1] & 0x0f) << 2) + ((char_array_3[2] & 0xc0) >> 6);
			char_array_4[3] = char_array_3[2] & 0x3f;
            
			for (j = 0; (j < i + 1); j++)
				ret += base64_chars[char_array_4[j]];
            
			while ((i++ < 3))
				ret += '=';
            
		}
        
		return ret;
	}
    
	std::string base64_decode(std::string const& encoded_string)
	{
		int in_len = encoded_string.size();
		int i = 0;
		int j = 0;
		int in_ = 0;
		unsigned char char_array_4[4], char_array_3[3];
		std::string ret;
        
		while (in_len-- && (encoded_string[in_] != '=') && is_base64(encoded_string[in_]))
		{
			char_array_4[i++] = encoded_string[in_]; in_++;
			if (i == 4)
			{
				for (i = 0; i <4; i++)
					char_array_4[i] = base64_chars.find(char_array_4[i]);
                
				char_array_3[0] = (char_array_4[0] << 2) + ((char_array_4[1] & 0x30) >> 4);
				char_array_3[1] = ((char_array_4[1] & 0xf) << 4) + ((char_array_4[2] & 0x3c) >> 2);
				char_array_3[2] = ((char_array_4[2] & 0x3) << 6) + char_array_4[3];
                
				for (i = 0; (i < 3); i++)
					ret += char_array_3[i];
				i = 0;
			}
		}
        
		if (i)
		{
			for (j = i; j <4; j++)
				char_array_4[j] = 0;
            
			for (j = 0; j <4; j++)
				char_array_4[j] = base64_chars.find(char_array_4[j]);
            
			char_array_3[0] = (char_array_4[0] << 2) + ((char_array_4[1] & 0x30) >> 4);
			char_array_3[1] = ((char_array_4[1] & 0xf) << 4) + ((char_array_4[2] & 0x3c) >> 2);
			char_array_3[2] = ((char_array_4[2] & 0x3) << 6) + char_array_4[3];
            
			for (j = 0; (j < i - 1); j++) ret += char_array_3[j];
		}
        
		return ret;
	}
    
	std::string getHMAC (std::string nonce, std::string appSecret)
	{
		const unsigned int LEN = 64;
		unsigned int len = LEN;
		unsigned char result[LEN] = { 0 };
        
        HMAC_CTX ctx;
        HMAC_CTX_init(&ctx);
        
        HMAC_Init_ex(&ctx, appSecret.c_str(), appSecret.length(), EVP_sha256(), NULL);
        HMAC_Update(&ctx, (unsigned char*)nonce.c_str(), nonce.length());
        HMAC_Final(&ctx, result, &len);
        HMAC_CTX_cleanup(&ctx);
        
        std::string str = base64_encode( result, len ).c_str();
        
        return str;
	}

	char tempLog[1024];


	//
	//	Code below came from: http://stackoverflow.com/questions/2915672/snprintf-and-visual-studio-2010
	//	Need to move somwhere else
	//
	#ifdef _MSC_VER
	#include <stdarg.h>
	#define snprintf c99_snprintf

	inline int c99_vsnprintf(char* str, size_t size, const char* format, va_list ap)
	{
		int count = -1;

		if (size != 0)
			count = _vsnprintf_s(str, size, _TRUNCATE, format, ap);
		if (count == -1)
			count = _vscprintf(format, ap);

		return count;
	}

	inline int c99_snprintf(char* str, size_t size, const char* format, ...)
	{
		int count;
		va_list ap;

		va_start(ap, format);
		count = c99_vsnprintf(str, size, format, ap);
		va_end(ap);

		return count;
	}
	#endif
	//	End of Stack overflow code
}

namespace gamesparks
{

struct GSObject::pimpl
{
	WebSocket::pointer ws;
};

GSObject::GSObject ()
	: m_Data(0) 
	, m_RequestClose(false)
	, m_isAuthenticated(false)
	, logUserData(0)
	, logCallback(0)
    , interval( 30 )
    , keepAliveTimer( interval )
	, storeData(0)
	, retrieveData(0)
	, retrieveCallback(0)
	, storeCallback(0)
{
	memset (m_Callbacks, 0, sizeof(m_Callbacks));
	m_Data = new GSObject::pimpl();
	m_ConnectionTarget = TargetServer::keLoadBalancer;

}

GSObject::~GSObject()
{
	if ( m_Data != 0 ) delete m_Data;
}

bool GSObject::doConnect ()
{
	if ( m_Data->ws == 0 )
	{
		//
		//	This is where we can see who to connect to using an enum
		//
		WebSocket::pointer ws;
		if ( m_ConnectionTarget == TargetServer::keLoadBalancer )
		{
			ws = WebSocket::from_url(loadBlancerURL);
		}
		else
		{
			ws = WebSocket::from_url(serverURL);
		}

		if ( ws == 0 )
		{
			//Failed to create websocket
			return false;
		}

		m_Data->ws = ws;
		return true;
	}
	//We already have a connection open
	return false;
}

void GSObject::close()
{
	if ( m_Data->ws != 0 )
	{
		m_RequestClose = true;
	}	
}

bool GSObject::isAuthenticated()
{
	return (isConnected() && m_isAuthenticated);
}

bool GSObject::isConnected()
{
	return (!m_RequestClose && m_Data->ws != 0);
}

bool GSObject::sendJsonMessage ( Json::Value & message )
{
	if ( ! m_Data->ws ) return false;
    
    Json::FastWriter writer;
	std::string outputConfig = writer.write( message );

	std::string prefix = "Send: ";
	logOut ((prefix + outputConfig).c_str());

	m_Data->ws->send(outputConfig);

	return true;
}

bool GSObject::sendStrMessage ( const std::string &message )
{
	if ( ! m_Data->ws ) return false;

	//TODO: Make this more efficient - ouch
	std::string prefix = "Send: ";
	logOut ((prefix + message).c_str());

	m_Data->ws->send(message);
	return true;
}

bool GSObject::connect (std::string URL, std::string appSecret)
{
	//We dont want to change anything if we are already connected
	if ( m_Data->ws ) return false;

	m_RequestClose = false;

	loadBlancerURL = URL;
	m_appSecret = appSecret;

    std::string logMsg = "Connecting to:";
    logOut((logMsg + URL).c_str());
    
	return doConnect();
}
    
void GSObject::setKeepAliveInterval( float _interval )
{
    interval = _interval;
}
    
void GSObject::disconnect()
{
    m_RequestClose = false;
    m_ConnectionTarget = TargetServer::keLoadBalancer;
    m_Data->ws->close();
    
    //Cleanup websocket
    delete m_Data->ws;
    m_Data->ws = 0;
    
    std::string message = "{}";
    Json::Value dummy;
    m_CallCallback(CallbackType::keOnDisconnect, message, dummy);
}

bool GSObject::update ( float dt )
{
	WebSocket::pointer ws = m_Data->ws;
	

	if ( ws != 0 )
	{
		if ( m_RequestClose )
		{
			disconnect();
			return false;
		}

		if (ws->getReadyState() != WebSocket::CLOSED) 
		{
			ws->poll();
	    	ws->dispatch(wsMessageCB, this);
            
            //send keep alive every 30 sec ( dependant on user spec )
            keepAliveTimer -= dt;
            if( keepAliveTimer <= 0 )
            {
                keepAliveTimer = interval;
                sendStrMessage( "" );
            }
		}
		else
		{
			//We must have just been disconnected
			logOut ("Disconnected from WS");
			delete m_Data->ws;
			m_Data->ws = 0;

			//
			//	Now lets check our error status and reconnect
			//
			if ( m_ConnectionTarget == TargetServer::keLoadBalancer )
			{
				m_ConnectionTarget = TargetServer::keServer;
				doConnect();
			}
			else
			{
				//Booted off after a handshake, lets inform our callback
				std::string message = "{}";
				Json::Value dummy;
				m_CallCallback(CallbackType::keOnDisconnect, message, dummy);
			}
		}
	}
	else
	{
		//We have no current connection open
	}

	return ws != 0; //Do we have a connection open?
}

void GSObject::wsMessageCB (const std::string & message, void* userData)
{
	GSObject *self = (GSObject*)userData;
	self->logOut (message.c_str());
	self->handleMessage(message);
}

void GSObject::handleMessage (const std::string & message)
{
	snprintf (tempLog, sizeof(tempLog), "%s\n", message.c_str());
//	logOut(tempLog);

	Json::Value response;
	Json::Reader reader;
    
	bool parsingSuccessful = reader.parse( message.c_str(), response );
	if ( !parsingSuccessful )
	{
	    // report to the user the failure and their locations in the document.
	    snprintf (tempLog, sizeof(tempLog), "Failed to parse configuration\n%s", reader.getFormattedErrorMessages().c_str());
	    logOut(tempLog);
	    return;
	}

	if ( response["@class"].asString() == ".AuthenticatedConnectResponse" )
	{
        if ( response.isMember("error") )
        {
            logOut("Gamesparks error: \n");
            m_Data->ws->close();
        }
        else
        {
        	handleHandshake(response, message);
        }
	}
	else if ( response["@class"].asString() == ".AuthenticationResponse" )
	{
		if ( !response.isMember("error")  )
		{
			if ( response.isMember("authToken") )
			{
				storeValue("GS_AUTH_TOKEN", response[ "authToken" ].asString() );
			}
			m_isAuthenticated = true;
		}
		else
		{ 
			//Auth error
			m_isAuthenticated = false;
		}
		

		m_CallCallback(CallbackType::keOnMessage, message, response);
	}
	else
	{
		m_CallCallback(CallbackType::keOnMessage, message, response);
	}
}
    
void GSObject::handleHandshake (Json::Value& response, const std::string & strResponse)
{
	if ( m_ConnectionTarget == TargetServer::keLoadBalancer )
	{
		serverURL = (response.isMember("connectUrl")) ? response[ "connectUrl" ].asString() : "";

		return;
	}
	else if (response.isMember("nonce"))
	{
		std::string hmac = getHMAC(response[ "nonce" ].asString(), m_appSecret);
        
        Json::Value nonceReturn;
		nonceReturn["@class"] = ".AuthenticatedConnectRequest";
		nonceReturn["hmac"] = hmac;
		nonceReturn["platform"] = m_Platform;
		nonceReturn["os"] = m_OS;

		if (sessionId != "")
		{
            nonceReturn["sessionId"] = sessionId;
		}

		std::string authToken = retrieveValue("GS_AUTH_TOKEN");
		if (authToken != "")
		{
            nonceReturn["authToken"] = authToken;
		}

		//We need to put the proper data in here
		Json::FastWriter writer;
		std::string outputConfig = writer.write( nonceReturn );

		//TODO: Make this more efficient - ouch
		std::string prefix = "Send: ";
		logOut ((prefix + outputConfig).c_str());

		m_Data->ws->send(outputConfig);

		return;
	} 
	else if (response.isMember("sessionId"))
	{
		//Store the session info here
		sessionId = response[ "sessionId" ].asString();

		//
		//	We are re-connecting with a valid session
		//
		if (response.isMember("authToken"))
		{
			storeValue("GS_AUTH_TOKEN", response[ "authToken" ].asString() );
			m_isAuthenticated = true;
		}

		//Here we have successfully connected, fire callback
		m_CallCallback(CallbackType::keOnConnect, strResponse, response);
	}
}

void GSObject::setPlatform(	std::string platform,
						std::string os )
{
	m_Platform = platform;
	m_OS = os;
}

void GSObject::setCallback (	GSObject::CallbackType::Enum cb, 
								GSObjCallback callback, 
								void *userData )
{
	m_Callbacks[cb].cb = callback;
	m_Callbacks[cb].userData = userData;
}

void GSObject::setLogCallback ( GSLogCallback cb, void *userData )
{
	logCallback = cb;
	logUserData = userData;
}

void GSObject::logOut ( const char * str )
{
	if ( logCallback != 0 )
	{
			logCallback (str, logUserData);
	}	
}

void GSObject::m_CallCallback(	CallbackType::Enum callback, 
								const std::string & str,
								Json::Value & json )
{
	GSCallbackData &cCallback = m_Callbacks[callback];
	CallbackData cData (str, json);

	if ( cCallback.cb != 0 )
	{
		cCallback.cb(
			cData,
			cCallback.userData
		);
	}
}

std::string GSObject::retrieveValue(std::string key)
{
	const char *retVal = 
		(retrieveCallback != 0) 
			? retrieveCallback(key.c_str(), retrieveData) 
			: 0;

	return (retVal != 0) ? std::string(retVal) : "";
}
void GSObject::storeValue(std::string key, std::string value)
{
	if (storeCallback != 0)
	{
		storeCallback(key.c_str(), value.c_str(), storeData);
	}
}

void GSObject::setRetrieveCallback(GSRetrieveValueCallback cb, void *userData)
{
	retrieveCallback = cb;
	retrieveData = userData;
}

void GSObject::setStoreCallback(GSStoreValueCallback cb, void *userData)
{
	storeCallback = cb;
	storeData = userData;
}

bool GSObject::suspend()
{
    logOut( "Suspend: closing socket." );
    disconnect();
    return true;
}
    
bool GSObject::resume()
{
    logOut( "Resume: openning socket." );
    return true;
}
    
}
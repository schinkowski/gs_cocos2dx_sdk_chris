#ifndef __H_GSOBJECT
#define __H_GSOBJECT

#include <string>

namespace Json {
	class Value;
}

namespace gamesparks
{

struct CallbackData
{
	const std::string & str;
	Json::Value & json;

	CallbackData (	const std::string & _str,
		Json::Value & _json)
		: str(_str), json(_json) {};
};

class GSObject 
{
public:
	struct CallbackType {
		enum Enum {
			keOnConnect,
			keOnDisconnect,
			keOnMessage,
			keOnError,

			keCount
		};
	};
	struct TargetServer
	{
		enum Enum {
			keLoadBalancer,
			keServer
		};
	};
	
	typedef void (*GSObjCallback)(const CallbackData & response, void*);
	typedef void (*GSLogCallback)(const char *, void*);

	typedef void(*GSStoreValueCallback)(const char *, const char *, void*);
	typedef const char*(*GSRetrieveValueCallback)(const char *, void*);

	GSObject ();
	~GSObject();

	bool connect (	std::string URL,
					std::string appSecret );
	
	bool sendJsonMessage ( Json::Value & message );
	bool sendStrMessage ( const std::string & message );

	//This will reset authentication IDs
	void close();
	bool update( float dt );

	bool isAuthenticated();

	void setCallback (	GSObject::CallbackType::Enum cb, 
						GSObjCallback callback, 
						void *userData );

	void setLogCallback ( GSLogCallback cb, void *userData );

	void setStoreCallback(GSStoreValueCallback cb, void *userData);
	void setRetrieveCallback(GSRetrieveValueCallback cb, void *userData);

    void setKeepAliveInterval( float _interval );
    
	//
	//Not implemented
	//
	void setPlatform(	std::string platform,
						std::string os );

	bool suspend();
	bool resume();
private:
	struct pimpl;
	GSObject::pimpl *m_Data;

	std::string loadBlancerURL;
	std::string serverURL;
	std::string m_appSecret;

	std::string sessionId;

	std::string m_Platform;
	std::string m_OS;

	bool m_isAuthenticated;
	bool m_RequestClose;

	TargetServer::Enum m_ConnectionTarget;
private:
	bool isConnected();
    void disconnect();

	static void wsMessageCB(const std::string & message,
							void * userData);

	void handleMessage (const std::string & message);
	void handleHandshake (	Json::Value & response,
							const std::string & strResponse);

	bool doConnect ();
private:
	struct GSCallbackData {
		GSObjCallback cb;
		void *userData;
	};

	GSCallbackData m_Callbacks[CallbackType::keCount];

	void m_CallCallback(CallbackType::Enum callback, 
						const std::string & str,
						Json::Value & json );

	void *logUserData;
	GSLogCallback logCallback;

	void *storeData;
	void *retrieveData;
	GSStoreValueCallback storeCallback;
	GSRetrieveValueCallback retrieveCallback;

	void logOut ( const char * str );
    
	std::string retrieveValue(std::string key);
	void storeValue(std::string key, std::string value);

    float interval;
    float keepAliveTimer;
private:
	//Disable copy and assign
	GSObject & operator=(const GSObject&);
	GSObject(const GSObject&);
};

}
#endif
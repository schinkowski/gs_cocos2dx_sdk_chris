#ifndef IGameSparks_h__
#define IGameSparks_h__


namespace Json {
    class Value;
};

namespace GameSparks
{
	class IGameSparks
	{
	public:
		typedef void(*GSConnectionCB)(void *data);
		typedef const char* (*GSRequestIDGen)(void *data);
		typedef void(*GSMessageCB)(Json::Value&, void *data);
		
		typedef void(*GSLogCallback)(const char *, void*);
		typedef void(*GSStoreValueCallback)(const char *, const char *, void*);
		typedef const char*(*GSRetrieveValueCallback)(const char *, void*);

		struct Status
		{
			enum Enum {
				keDisconnected,
				keConnecting,
				keConnected,

				keCount
			};
		};

		struct TargetEndpoint
		{
			enum Enum {
				kePreview,
				keProduction,

				keCount
			};
		};

		static IGameSparks *Create();

		static bool Init();
		static void Cleanup();

		//Non static methods here
		virtual bool sendMessage(const char*  message,
			GSMessageCB callback = 0,
			unsigned int timeout = 0,
			void *userData = 0) = 0;

		virtual bool sendMessageWithData(const char*  message,
			Json::Value& data,
			GSMessageCB callback = 0,
			unsigned int timeout = 0,
			void *userData = 0) = 0;

		virtual void setRequestIDGen(GSRequestIDGen nGenerator, void *data) = 0;

		//
		//	Global callbacks
		//
		virtual void setOnError(GSMessageCB callback, void *data) = 0;
		virtual void setOnMessage(GSMessageCB callback, void *data) = 0;
		virtual void setOnConnect(GSConnectionCB callback, void *data) = 0;
		virtual void setOnDisconnect(GSConnectionCB callback, void *data) = 0;

		virtual void setLogCallback(GSLogCallback cb, void *userData) = 0;
		virtual void setKeepAliveInterval(float _interval) = 0;
		virtual void setPlatform(const char*  platform, const char*  os) = 0;

		virtual bool isAuthenticated() = 0;

		virtual void setStoreCallback(GSStoreValueCallback cb, void *userData) = 0;
		virtual void setRetrieveCallback(GSRetrieveValueCallback cb, void *userData) = 0;
		//Clear pending requests
		//Get pending requests

		virtual Status::Enum getStatus() = 0;

		virtual void close() = 0;
		virtual bool suspend() = 0;
		virtual bool resume() = 0;
		virtual bool connect(const char*  target, const char*  secret, TargetEndpoint::Enum endpoint) = 0;

		virtual void update(int elapsedTimeMS) = 0; // - this will timeout requests
	};
}


#endif // IGameSparks_h__
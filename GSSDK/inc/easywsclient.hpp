#ifndef EASYWSCLIENT_HPP_20120819_MIOFVASDTNUASZDQPLFD
#define EASYWSCLIENT_HPP_20120819_MIOFVASDTNUASZDQPLFD
    
    // This code comes from:
    // https://github.com/dhbaird/easywsclient
    //
    // To get the latest version:
    // wget https://raw.github.com/dhbaird/easywsclient/master/easywsclient.hpp
    // wget https://raw.github.com/dhbaird/easywsclient/master/easywsclient.cpp
    
#include <string>
    
#ifdef SSL_SUPPORT
#include <openssl/ssl.h>
#include <openssl/err.h>
#include <openssl/crypto.h>
#endif
    
    namespace easywsclient {
        
        bool initEasyWSClient ();
        void cleanupEasyWSClient ();
        
        class WebSocket {
        public:
            typedef void (*WSMessageCallback)(const std::string &, void*);
            typedef WebSocket * pointer;
            typedef enum readyStateValues { CLOSING, CLOSED, CONNECTING, OPEN } readyStateValues;
            
            // Factories:
            static pointer create_dummy();
            static pointer from_url(const std::string& url, const std::string& origin = std::string());
            static pointer from_url_no_mask(const std::string& url, const std::string& origin = std::string());
            
            // Interfaces:
            virtual ~WebSocket() { }
            virtual void poll(int timeout = 0) = 0; // timeout in milliseconds
            virtual void send(const std::string& message) = 0;
            virtual void sendPing() = 0;
            virtual void close() = 0;
            virtual readyStateValues getReadyState() const = 0;
            
            void dispatch(WSMessageCallback callback, void* userData) {
                _dispatch(callback, userData);
            }

            
        protected:
            struct Callback { virtual void operator()(const std::string& message) = 0; };
            virtual void _dispatch(WSMessageCallback callback, void* data) = 0;
        };
        
    } // namespace easywsclient
    
#endif /* EASYWSCLIENT_HPP_20120819_MIOFVASDTNUASZDQPLFD */

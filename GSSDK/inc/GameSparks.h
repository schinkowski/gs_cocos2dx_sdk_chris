#ifndef GameSparks_h__
#define GameSparks_h__

#include "IGameSparks.h"
#include <string>
#include <map>
#include <vector>
namespace gamesparks { class GSObject; struct CallbackData; }

namespace Json {
    class Value;
}

namespace GameSparks
{
	class CGamesparks  : public IGameSparks
	{
	public:

		//	Accessible through getPendingRequests
		//
		struct gsRequest
		{
            std::string requestID;
			GSMessageCB callback;
			unsigned int timeout;
			void *data;
		};

		//Non static methods here
		bool sendMessage(const char*  message,
							GSMessageCB callback = 0, 
							unsigned int timeout = 0,
							void *userData = 0);

		bool sendMessageWithData(const char*  message,
									Json::Value& data,
									GSMessageCB callback = 0, 
									unsigned int timeout = 0,
									void *userData = 0);

		void setRequestIDGen (GSRequestIDGen nGenerator, void *data);

		//
		//	Global callbacks
		//
		void setOnError (GSMessageCB callback, void *data);
		void setOnMessage (GSMessageCB callback, void *data);
		void setOnConnect (GSConnectionCB callback, void *data);
		void setOnDisconnect (GSConnectionCB callback, void *data);

		void setLogCallback (GSLogCallback cb, void *userData);
        void setKeepAliveInterval( float _interval );
		void setPlatform(const char*  platform, const char*  os);
        
		//Clear pending requests
		//Get pending requests

		Status::Enum getStatus ();

		void close ();
		bool suspend ();
		bool resume ();
		bool connect(const char*  target, const char*  secret, TargetEndpoint::Enum endpoint);

		bool isAuthenticated();

		void setStoreCallback(GSStoreValueCallback cb, void *userData);
		void setRetrieveCallback(GSRetrieveValueCallback cb, void *userData);

		void update(int elapsedTimeMS); // - this will timeout requests

		static IGameSparks *createConcrete();

		~CGamesparks();
	private:
		typedef std::map<std::string, gsRequest> requestList;

        std::string appSecret;
        std::string apiKey;
        TargetEndpoint::Enum targetEndPoint;
        
		requestList m_PendingRequests;
		gamesparks::GSObject *m_gsObject;

		Status::Enum m_Status;

		//
		//	Request ID generation
		//
		GSRequestIDGen m_CurrentRequestGen;
		void *m_RequestGenData;

		//Should be large enough!
		unsigned long long m_CurrentRequestID;//@ 1,000,000,000 a second there are enough IDs for 292 years
		static std::string m_GenRequest(void *data);

		//User callbacks
		GSConnectionCB m_OnConnectCB;
		GSConnectionCB m_OnDisconnectCB;
		GSMessageCB m_OnErrorCB;
		GSMessageCB m_OnMessageCB;

		void *m_OnConnectData;
		void *m_OnDisconnectData;
		void *m_OnErrorData;
		void *m_OnMessageData;

		//callback Member methods
		void m_OnConnect (Json::Value& data);
		void m_OnMessage(Json::Value& data);
		void m_OnError(Json::Value& data);
		void m_OnDisconnect(Json::Value& data);

		//Callback handlers
		static void cb_connectCallback (
			const gamesparks::CallbackData & response, 
			void *self);

		static void cb_messageCallback (
			const gamesparks::CallbackData & response, 
			void *self);

		static void cb_errorCallback (
			const gamesparks::CallbackData & response, 
			void *self);

		static void cb_disconnectCallback (
			const gamesparks::CallbackData & response, 
			void *self);
	private:
		CGamesparks (gamesparks::GSObject *_gsObject);

		//No assignment or copy
		CGamesparks & operator=(const CGamesparks&);
		CGamesparks(const CGamesparks&);
	};
}

#endif // GameSparks_h__
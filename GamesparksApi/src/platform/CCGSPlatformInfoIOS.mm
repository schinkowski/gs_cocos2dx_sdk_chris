//
//  CCGSPlatformInfoIOS.cpp
//  GameSparksTest
//
//  Created by Tomasz Kandziora on 26/04/2014.
//
//

#include "CCGSPlatformInfo.h"
#include "platform/CCPlatformConfig.h"

#if CC_TARGET_PLATFORM == CC_PLATFORM_IOS

#import <UIKit/UIKit.h>

namespace GameSparks
{
    std::string getDeviceId()
    {
        NSString* str = [ [ [ UIDevice currentDevice ] identifierForVendor ] UUIDString ];
        std::string retStr = [ str UTF8String ];
        return retStr;
    }

    std::string getDeviceModel()
    {
        NSString* str = [ [ UIDevice currentDevice ] model ];
        std::string retStr = [ str UTF8String ];
        return retStr;
    }

    std::string getDeviceName()
    {
        NSString* str = [ [ UIDevice currentDevice ] name ];
        std::string retStr = [ str UTF8String ];
        return retStr;
    }

    std::string getDeviceOS()
    {
        NSString* str = [ [ UIDevice currentDevice ] systemName ];
        std::string retStr = [ str UTF8String ];
        return retStr;
    }

    std::string getDeviceType()
    {
        return "";
    }
}
#endif //CC_PLATFORM_IOS
//
//  CCGSPlatformInfoNoSupport.cpp
//  GameSparksTest
//
//  Created by Tomasz Kandziora on 26/04/2014.
//
//

#include "CCGSPlatformInfo.h"
#include "platform/CCPlatformConfig.h"

#if CC_TARGET_PLATFORM != CC_PLATFORM_IOS\
	&& CC_TARGET_PLATFORM != CC_PLATFORM_ANDROID\
	&& CC_TARGET_PLATFORM != CC_PLATFORM_WP8

namespace GameSparks
{
    std::string getDeviceId()
    {
        return "";
    }
    
    std::string getDeviceModel()
    {
        return "";
    }
    
    std::string getDeviceName()
    {
        return "";
    }
    
    std::string getDeviceOS()
    {
        return "";
    }
    
    std::string getDeviceType()
    {
        return "";
    }
}

#endif //CC_PLATFORM_IOS
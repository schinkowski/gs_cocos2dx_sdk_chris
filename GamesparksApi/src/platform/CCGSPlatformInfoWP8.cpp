//
//  CCGSPlatformInfoAndroid.cpp
//  GameSparksTest
//
//  Created by Tomasz Kandziora on 26/04/2014.
//
//

#include "CCGSPlatformInfo.h"
#include "platform/CCPlatformConfig.h"

#if CC_TARGET_PLATFORM == CC_PLATFORM_WP8

#include <windows.h>

namespace GameSparks
{
	std::string platformStringToChar(Platform::String ^str)
	{
		std::wstring wstr(str->Data());

		int len = WideCharToMultiByte(CP_ACP, 0, wstr.c_str(), wstr.size(), NULL, 0, NULL, NULL);
		char* buffer = new char[len + 1];
		WideCharToMultiByte(CP_ACP, 0, wstr.c_str(), wstr.size(), buffer, len, NULL, NULL);
		buffer[len] = '\0';

		std::string returnValue(buffer);
		delete[] buffer;

		return returnValue;
	}

	//Needs: ID_CAP_IDENTITY_DEVICE set in manifest
	std::string getDeviceId()
    {
        Platform::String ^udid = Windows::Phone::System::Analytics::HostInformation::PublisherHostId;
		return platformStringToChar(udid);
    }

    std::string getDeviceModel()
    {
        return "";
    }

    std::string getDeviceName()
    {
        return "";
    }

    std::string getDeviceOS()
    {
        return "WP8";
    }

    std::string getDeviceType()
    {
        return "";
    }
}
#endif //CC_PLATFORM_IOS
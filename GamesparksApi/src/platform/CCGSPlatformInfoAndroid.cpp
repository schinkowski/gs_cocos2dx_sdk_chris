//
//  CCGSPlatformInfoAndroid.cpp
//  GameSparksTest
//
//  Created by Tomasz Kandziora on 26/04/2014.
//
//

#include "CCGSPlatformInfo.h"
#include "platform/CCPlatformConfig.h"

#if CC_TARGET_PLATFORM == CC_PLATFORM_ANDROID

#include "platform/android/jni/JniHelper.h"

using namespace cocos2d;

namespace GameSparks
{
    std::string getStringForJavaMethod( std::string methodName )
    {
        JniMethodInfo t;
        if( JniHelper::getStaticMethodInfo( t
            ,"com/gamesparks/example/GameSparks"
            ,methodName.c_str()
            ,"()Ljava/lang/String;" ) )
        {
            jstring jstr = NULL; 
            jstr = (jstring)t.env->CallStaticObjectMethod(t.classID, t.methodID);
            char* cstr = NULL; 
            cstr = (char*) t.env->GetStringUTFChars(jstr, 0);
            std::string ret(cstr);
            t.env->ReleaseStringUTFChars(jstr, cstr);
            t.env->DeleteLocalRef(jstr);
            return ret.c_str();
        }

        return "";
    }

    std::string getDeviceId()
    {
		return getStringForJavaMethod( "getDeviceId" );
    }

    std::string getDeviceModel()
    {
        return getStringForJavaMethod( "getDeviceModel" );
    }

    std::string getDeviceName()
    {
        return getStringForJavaMethod( "getDeviceName" );
    }

    std::string getDeviceOS()
    {
        return getStringForJavaMethod( "getDeviceOS" );
    }

    std::string getDeviceType()
    {
        return getStringForJavaMethod( "getDeviceType" );
    }
}
#endif //CC_PLATFORM_IOS
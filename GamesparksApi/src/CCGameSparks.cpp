#include "CCGameSparks.h"
#include "IGameSparks.h"
#include "CCMacros.h"
#include "json.h"
#include "CCGSPlatformInfo.h"
#include "cocos2d.h"

namespace GameSparks
{
    CCGamesparks* CCGamesparks::instance = 0;

	namespace
	{
		std::string token = "";		// really don't like it, but has to be for WP8
	}
    
    void setAuthenticationTokenCallback( const char *key, const char *val, void *data )
    {
		token = val;
        cocos2d::CCUserDefault::sharedUserDefault()->setStringForKey( key, token );
		cocos2d::CCUserDefault::sharedUserDefault()->flush();
    }
    
    const char* getAuthenticationTokenCallback( const char *key, void *data )
    {
		token = cocos2d::CCUserDefault::sharedUserDefault()->getStringForKey( key );
        return token.c_str();
    }
    
    CCGamesparks::CCGamesparks() :
        REQUEST_TIMEOUT( 60 ),
        interval( 1.0 ),
        unitsPerSecond( 1.0 ),
        gsClient( 0 )
    {

    }

    CCGamesparks::~CCGamesparks()
    {
        
    }

    CCGamesparks* CCGamesparks::create()
    {
        CCAssert( instance == 0, "WARNING: GameSparks already created. Will not be initialised second time" );
        
        if( !instance )
        {
            instance = new CCGamesparks();
            GameSparks::IGameSparks::Init();
            instance->gsClient = GameSparks::IGameSparks::Create();
            instance->gsClient->setPlatform( getPlatformString().c_str(), getDeviceOS().c_str() );
            instance->gsClient->setKeepAliveInterval( 30.0 );
            instance->gsClient->setStoreCallback( setAuthenticationTokenCallback, 0 );
            instance->gsClient->setRetrieveCallback( getAuthenticationTokenCallback, 0 );
            cocos2d::CCDirector::sharedDirector()->getScheduler()->scheduleSelector( cocos2d::SEL_SCHEDULE( &CCGamesparks::update ),
                                                                                     instance,
                                                                                     instance->interval,
                                                                                     false
                                                                                    );
        }

        return instance;
    }

    void CCGamesparks::connect( std::string apiKey, std::string apiSecret, ConnectionMode::Enum mode )
    {
        gsClient->connect( apiKey.c_str(), apiSecret.c_str(), IGameSparks::TargetEndpoint::kePreview);
    }
    
    void CCGamesparks::update( float dt )
    {
        gsClient->update( dt / unitsPerSecond );
    }

    CCGamesparks* CCGamesparks::getInstance()
    {
        CCAssert(instance != NULL, "Error: Initialize the client before you try to use it.");
        return instance;
    }

    void CCGamesparks::destroyInstance()
    {
        if( instance )
        {
            instance->gsClient->close();
            IGameSparks::Cleanup();
            
            delete instance;
            instance = 0;
        }
    }
    
    void CCGamesparks::setUnitsPerSecond( double _units )
    {
        unitsPerSecond = _units;
    }
    
    void CCGamesparks::setUpdateInterval( double _interval )
    {
        interval = _interval;
        cocos2d::CCDirector::sharedDirector()->getScheduler()->unscheduleSelector( cocos2d::SEL_SCHEDULE( &CCGamesparks::update ), instance );
        cocos2d::CCDirector::sharedDirector()->getScheduler()->scheduleSelector( cocos2d::SEL_SCHEDULE( &CCGamesparks::update ), instance, interval, false );
    }
    
    void CCGamesparks::setLogCallback ( IGameSparks::GSLogCallback cb, void *userData)
    {
        gsClient->setLogCallback( cb, userData );
    }
    
    void CCGamesparks::suspend()
    {
        this->gsClient->suspend();
    }
    
    void CCGamesparks::resume()
    {
        this->gsClient->resume();
    }
    
    bool CCGamesparks::isAuthenticated()
    {
        return gsClient->isAuthenticated();
    }
    
    void CCGamesparks::setOnError ( IGameSparks::GSMessageCB callback, void *data )
    {
        this->gsClient->setOnError( callback, data );
    }
    
    void CCGamesparks::setOnMessage ( IGameSparks::GSMessageCB callback, void *data )
    {
        this->gsClient->setOnMessage( callback, data );
    }
    
    void CCGamesparks::setOnConnect ( IGameSparks::GSConnectionCB callback, void *data )
    {
        this->gsClient->setOnConnect( callback, data );
    }
    
    void CCGamesparks::setOnDisconnect ( IGameSparks::GSConnectionCB callback, void *data )
    {
        this->gsClient->setOnDisconnect( callback, data );
    }
    
    void CCGamesparks::sendMessageWithData(std::string message,
                                           Json::Value& root,
                                           IGameSparks::GSMessageCB callback,
                                           void *userData )
    {
        gsClient->sendMessageWithData( message.c_str(), root, callback, REQUEST_TIMEOUT, userData );
    }
}
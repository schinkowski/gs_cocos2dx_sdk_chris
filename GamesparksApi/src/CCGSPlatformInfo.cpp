//
//  FCCGSPlatformInfo.cpp
//  GameSparksTest
//
//  Created by Tomasz Kandziora on 26/04/2014.
//
//

#include "CCGSPlatformInfo.h"
#include "platform/CCPlatformConfig.h"

namespace GameSparks
{ 
    std::string getPlatformString()
    {
        switch ( CC_TARGET_PLATFORM )
        {
            case CC_PLATFORM_WIN32:     return "Windows";
            case CC_PLATFORM_LINUX:     return "Linux";
            case CC_PLATFORM_MAC:       return "OSX";
            case CC_PLATFORM_ANDROID:   return "Android";
            case CC_PLATFORM_IOS:       return "IOS";
            case CC_PLATFORM_BLACKBERRY:return "BB";
            case CC_PLATFORM_NACL:      return "NaCl";
            case CC_PLATFORM_EMSCRIPTEN:return "Emscripten";
            case CC_PLATFORM_TIZEN:     return "Tizen";
            case CC_PLATFORM_WINRT:     return "WinRT";
            case CC_PLATFORM_WP8:       return "WP8";
            case CC_PLATFORM_MARMALADE: return "Marmalade";
            default: return "Not known";
        }
    }
}
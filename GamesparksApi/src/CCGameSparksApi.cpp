//
//  CCGameSparksApi.cpp
//  GameSparksTest
//
//  Created by Tomasz Kandziora on 25/04/2014.
//
//

#include "CCGameSparksApi.h"
#include "CCGameSparks.h"
#include "GameSparks.h"
#include "json.h"
#include <vector>

namespace GameSparks
{
    template< typename T >
    Json::Value VecToJsonValue( const std::vector< T >& vec )
    {
        Json::Value root;
        
        typename std::vector< T >::const_iterator iter;
        for ( iter = vec.begin() ; iter != vec.end() ; ++iter)
        {
            std::string i = (*iter);
            root.append( i );
        }
     
        return root;
    }
    
	void CCGamesparksAPI::acceptChallengeRequest( std::string challengeInstanceId, std::string message, IGameSparks::GSMessageCB callback, void* userData )
	{
        Json::Value request;
		request["challengeInstanceId"] = challengeInstanceId;
		request["message"] = message;
        CCGamesparks::getInstance()->sendMessageWithData( ".AcceptChallengeRequest", request, callback, userData );
    }
    
	void CCGamesparksAPI::accountDetailsRequest( IGameSparks::GSMessageCB callback, void* userData )
	{
        Json::Value request;
        CCGamesparks::getInstance()->sendMessageWithData( ".AccountDetailsRequest", request, callback, userData );
    }
    
	void CCGamesparksAPI::analyticsRequest( const Json::Value& data, bool end, std::string key, bool start, IGameSparks::GSMessageCB callback, void* userData )
	{
        Json::Value request;
		request["data"] = data;
		request["end"] = end;
		request["key"] = key;
		request["start"] = start;
        CCGamesparks::getInstance()->sendMessageWithData( ".AnalyticsRequest", request, callback, userData );
    }
    
	void CCGamesparksAPI::aroundMeLeaderboardRequest( int count, const std::vector<std::string>& friendIds, std::string leaderboardShortCode, bool social, IGameSparks::GSMessageCB callback, void* userData )
	{
        Json::Value request;
		request["count"] = count;
		request["friendIds"] = VecToJsonValue< std::string >( friendIds );
		request["leaderboardShortCode"] = leaderboardShortCode;
		request["social"] = social;
        CCGamesparks::getInstance()->sendMessageWithData( ".AroundMeLeaderboardRequest", request, callback, userData );
    }
    
	void CCGamesparksAPI::authenticationRequest( std::string userName, std::string password, IGameSparks::GSMessageCB callback, void* userData )
	{
        Json::Value request;
		request["password"] = password;
		request["userName"] = userName;
        CCGamesparks::getInstance()->sendMessageWithData( ".AuthenticationRequest", request, callback, userData );
    }
    
	void CCGamesparksAPI::buyVirtualGoodsRequest( int currencyType, int quantity, std::string shortCode, IGameSparks::GSMessageCB callback, void* userData )
	{
        Json::Value request;
		request["currencyType"] = currencyType;
		request["quantity"] = quantity;
		request["shortCode"] = shortCode;
        CCGamesparks::getInstance()->sendMessageWithData( ".BuyVirtualGoodsRequest", request, callback, userData );
    }
    
	void CCGamesparksAPI::changeUserDetailsRequest( std::string displayName, IGameSparks::GSMessageCB callback, void* userData )
	{
        Json::Value request;
		request["displayName"] = displayName;
        CCGamesparks::getInstance()->sendMessageWithData( ".ChangeUserDetailsRequest", request, callback, userData );
    }
    
	void CCGamesparksAPI::chatOnChallengeRequest( std::string challengeInstanceId, std::string message, IGameSparks::GSMessageCB callback, void* userData )
	{
        Json::Value request;
		request["challengeInstanceId"] = challengeInstanceId;
		request["message"] = message;
        CCGamesparks::getInstance()->sendMessageWithData( ".ChatOnChallengeRequest", request, callback, userData );
    }
    
	void CCGamesparksAPI::consumeVirtualGoodRequest( int quantity, std::string shortCode, IGameSparks::GSMessageCB callback, void* userData )
	{
        Json::Value request;
		request["quantity"] = quantity;
		request["shortCode"] = shortCode;
        CCGamesparks::getInstance()->sendMessageWithData( ".ConsumeVirtualGoodRequest", request, callback, userData );
    }
    
	void CCGamesparksAPI::createChallengeRequest( std::string accessType, std::string challengeMessage, std::string challengeShortCode, int currency1Wager, int currency2Wager, int currency3Wager, int currency4Wager, int currency5Wager, int currency6Wager, int endTime, int expiryTime, int maxAttempts, int maxPlayers, int minPlayers, int startTime, const std::vector<std::string>& usersToChallenge, IGameSparks::GSMessageCB callback, void* userData )
	{
        Json::Value request;
		request["accessType"] = accessType;
		request["challengeMessage"] = challengeMessage;
		request["challengeShortCode"] = challengeShortCode;
		request["currency1Wager"] = currency1Wager;
		request["currency2Wager"] = currency2Wager;
		request["currency3Wager"] = currency3Wager;
		request["currency4Wager"] = currency4Wager;
		request["currency5Wager"] = currency5Wager;
		request["currency6Wager"] = currency6Wager;
		request["endTime"] = endTime;
		request["expiryTime"] = expiryTime;
		request["maxAttempts"] = maxAttempts;
		request["maxPlayers"] = maxPlayers;
		request["minPlayers"] = minPlayers;
		request["startTime"] = startTime;
		request["usersToChallenge"] = VecToJsonValue< std::string >( usersToChallenge );
        CCGamesparks::getInstance()->sendMessageWithData( ".CreateChallengeRequest", request, callback, userData );
    }
    
	void CCGamesparksAPI::declineChallengeRequest( std::string challengeInstanceId, std::string message, IGameSparks::GSMessageCB callback, void* userData )
	{
        Json::Value request;
		request["challengeInstanceId"] = challengeInstanceId;
		request["message"] = message;
        CCGamesparks::getInstance()->sendMessageWithData( ".DeclineChallengeRequest", request, callback, userData );
    }
    
	void CCGamesparksAPI::deviceAuthenticationRequest( std::string deviceId, std::string deviceModel, std::string deviceName, std::string deviceOS, std::string deviceType, std::string operatingSystem, IGameSparks::GSMessageCB callback, void* userData )
	{
        Json::Value request;
		request["deviceId"] = deviceId;
		request["deviceModel"] = deviceModel;
		request["deviceName"] = deviceName;
		request["deviceOS"] = deviceOS;
		request["deviceType"] = deviceType;
		request["operatingSystem"] = operatingSystem;
        CCGamesparks::getInstance()->sendMessageWithData( ".DeviceAuthenticationRequest", request, callback, userData );
    }
    
	void CCGamesparksAPI::dismissMessageRequest( std::string messageId, IGameSparks::GSMessageCB callback, void* userData )
	{
        Json::Value request;
		request["messageId"] = messageId;
        CCGamesparks::getInstance()->sendMessageWithData( ".DismissMessageRequest", request, callback, userData );
    }
    
	void CCGamesparksAPI::endSessionRequest( IGameSparks::GSMessageCB callback, void* userData )
	{
        Json::Value request;
        CCGamesparks::getInstance()->sendMessageWithData( ".EndSessionRequest", request, callback, userData );
    }
    
	void CCGamesparksAPI::facebookConnectRequest( std::string accessToken, std::string code, IGameSparks::GSMessageCB callback, void* userData )
	{
        Json::Value request;
		request["accessToken"] = accessToken;
		request["code"] = code;
        CCGamesparks::getInstance()->sendMessageWithData( ".FacebookConnectRequest", request, callback, userData );
    }
    
	void CCGamesparksAPI::findChallengeRequest( std::string accessType, int count, int offset, IGameSparks::GSMessageCB callback, void* userData )
	{
        Json::Value request;
		request["accessType"] = accessType;
		request["count"] = count;
		request["offset"] = offset;
        CCGamesparks::getInstance()->sendMessageWithData( ".FindChallengeRequest", request, callback, userData );
    }
    
	void CCGamesparksAPI::getChallengeRequest( std::string challengeInstanceId, std::string message, IGameSparks::GSMessageCB callback, void* userData )
	{
        Json::Value request;
		request["challengeInstanceId"] = challengeInstanceId;
		request["message"] = message;
        CCGamesparks::getInstance()->sendMessageWithData( ".GetChallengeRequest", request, callback, userData );
    }
    
	void CCGamesparksAPI::getDownloadableRequest( std::string shortCode, IGameSparks::GSMessageCB callback, void* userData )
	{
        Json::Value request;
		request["shortCode"] = shortCode;
        CCGamesparks::getInstance()->sendMessageWithData( ".GetDownloadableRequest", request, callback, userData );
    }
    
	void CCGamesparksAPI::getMessageRequest( std::string messageId, IGameSparks::GSMessageCB callback, void* userData )
	{
        Json::Value request;
		request["messageId"] = messageId;
        CCGamesparks::getInstance()->sendMessageWithData( ".GetMessageRequest", request, callback, userData );
    }
    
	void CCGamesparksAPI::getRunningTotalsRequest( const std::vector<std::string>& friendIds, std::string shortCode, IGameSparks::GSMessageCB callback, void* userData )
	{
        Json::Value request;
		request["friendIds"] = VecToJsonValue< std::string >( friendIds );
		request["shortCode"] = shortCode;
        CCGamesparks::getInstance()->sendMessageWithData( ".GetRunningTotalsRequest", request, callback, userData );
    }

	void CCGamesparksAPI::getUploadUrlRequest( const Json::Value& uploadData, IGameSparks::GSMessageCB callback, void* userData )
	{
        Json::Value request;
		request["uploadData"] = uploadData;
        CCGamesparks::getInstance()->sendMessageWithData( ".GetUploadUrlRequest", request, callback, userData );
    }
    
	void CCGamesparksAPI::getUploadedRequest( std::string uploadId, IGameSparks::GSMessageCB callback, void* userData )
	{
        Json::Value request;
		request["uploadId"] = uploadId;
        CCGamesparks::getInstance()->sendMessageWithData( ".GetUploadedRequest", request, callback, userData );
    }
    
	void CCGamesparksAPI::googlePlayBuyGoodsRequest( std::string currencyCode, std::string signature, std::string signedData, int subUnitPrice, IGameSparks::GSMessageCB callback, void* userData )
	{
        Json::Value request;
		request["currencyCode"] = currencyCode;
		request["signature"] = signature;
		request["signedData"] = signedData;
		request["subUnitPrice"] = subUnitPrice;
        CCGamesparks::getInstance()->sendMessageWithData( ".GooglePlayBuyGoodsRequest", request, callback, userData );
    }
    
	void CCGamesparksAPI::iOSBuyGoodsRequest( std::string currencyCode, std::string receipt, bool sandbox, int subUnitPrice, IGameSparks::GSMessageCB callback, void* userData )
	{
        Json::Value request;
		request["currencyCode"] = currencyCode;
		request["receipt"] = receipt;
		request["sandbox"] = sandbox;
		request["subUnitPrice"] = subUnitPrice;
        CCGamesparks::getInstance()->sendMessageWithData( ".IOSBuyGoodsRequest", request, callback, userData );
    }
    
	void CCGamesparksAPI::joinChallengeRequest( std::string challengeInstanceId, std::string message, IGameSparks::GSMessageCB callback, void* userData )
	{
        Json::Value request;
		request["challengeInstanceId"] = challengeInstanceId;
		request["message"] = message;
        CCGamesparks::getInstance()->sendMessageWithData( ".JoinChallengeRequest", request, callback, userData );
    }
    
	void CCGamesparksAPI::leaderboardDataRequest( std::string challengeInstanceId, int entryCount, const std::vector<std::string>& friendIds, std::string leaderboardShortCode, int offset, bool social, IGameSparks::GSMessageCB callback, void* userData )
	{
        Json::Value request;
		request["challengeInstanceId"] = challengeInstanceId;
		request["entryCount"] = entryCount;
		request["friendIds"] = VecToJsonValue< std::string >( friendIds );
		request["leaderboardShortCode"] = leaderboardShortCode;
		request["offset"] = offset;
		request["social"] = social;
        CCGamesparks::getInstance()->sendMessageWithData( ".LeaderboardDataRequest", request, callback, userData );
    }
    
	void CCGamesparksAPI::listAchievementsRequest( IGameSparks::GSMessageCB callback, void* userData )
	{
        Json::Value request;
        CCGamesparks::getInstance()->sendMessageWithData( ".ListAchievementsRequest", request, callback, userData );
    }
    
        Json::Value request;
	void CCGamesparksAPI::listChallengeRequest( int entryCount, int offset, std::string shortCode, std::string state, IGameSparks::GSMessageCB callback, void* userData )
	{
		request["entryCount"] = entryCount;
		request["offset"] = offset;
		request["shortCode"] = shortCode;
		request["state"] = state;
        CCGamesparks::getInstance()->sendMessageWithData( ".ListChallengeRequest", request, callback, userData );
    }
    
	void CCGamesparksAPI::listChallengeTypeRequest( IGameSparks::GSMessageCB callback, void* userData )
	{
        Json::Value request;
        CCGamesparks::getInstance()->sendMessageWithData( ".ListChallengeTypeRequest", request, callback, userData );
    }
    
	void CCGamesparksAPI::listGameFriendsRequest( IGameSparks::GSMessageCB callback, void* userData )
	{
        Json::Value request;
        CCGamesparks::getInstance()->sendMessageWithData( ".ListGameFriendsRequest", request, callback, userData );
    }
    
	void CCGamesparksAPI::listInviteFriendsRequest( IGameSparks::GSMessageCB callback, void* userData )
	{
        Json::Value request;
        CCGamesparks::getInstance()->sendMessageWithData( ".ListInviteFriendsRequest", request, callback, userData );
    }
    
	void CCGamesparksAPI::listLeaderboardsRequest( IGameSparks::GSMessageCB callback, void* userData )
	{
        Json::Value request;
        CCGamesparks::getInstance()->sendMessageWithData( ".ListLeaderboardsRequest", request, callback, userData );
    }
    
	void CCGamesparksAPI::listMessageRequest( int entryCount, int offset, IGameSparks::GSMessageCB callback, void* userData )
	{
        Json::Value request;
		request["entryCount"] = entryCount;
		request["offset"] = offset;
        CCGamesparks::getInstance()->sendMessageWithData( ".ListMessageRequest", request, callback, userData );
    }
    
	void CCGamesparksAPI::listMessageSummaryRequest( int entryCount, int offset, IGameSparks::GSMessageCB callback, void* userData )
	{
        Json::Value request;
		request["entryCount"] = entryCount;
		request["offset"] = offset;
        CCGamesparks::getInstance()->sendMessageWithData( ".ListMessageSummaryRequest", request, callback, userData );
    }
    
	void CCGamesparksAPI::listVirtualGoodsRequest( IGameSparks::GSMessageCB callback, void* userData )
	{
        Json::Value request;
        CCGamesparks::getInstance()->sendMessageWithData( ".ListVirtualGoodsRequest", request, callback, userData );
    }
    
	void CCGamesparksAPI::logChallengeEventRequest( std::string challengeInstanceId, std::string eventKey, IGameSparks::GSMessageCB callback, void* userData )
	{
        Json::Value request;
		request["challengeInstanceId"] = challengeInstanceId;
		request["eventKey"] = eventKey;
        CCGamesparks::getInstance()->sendMessageWithData( ".LogChallengeEventRequest", request, callback, userData );
    }
    
	void CCGamesparksAPI::logEventRequest( std::string eventKey, IGameSparks::GSMessageCB callback, void* userData )
	{
        Json::Value request;
		request["eventKey"] = eventKey;
        CCGamesparks::getInstance()->sendMessageWithData( ".LogEventRequest", request, callback, userData );
    }
    
	void CCGamesparksAPI::pushRegistrationRequest( std::string deviceOS, std::string pushId, IGameSparks::GSMessageCB callback, void* userData )
	{
        Json::Value request;
		request["deviceOS"] = deviceOS;
		request["pushId"] = pushId;
        CCGamesparks::getInstance()->sendMessageWithData( ".PushRegistrationRequest", request, callback, userData );
    }
    
	void CCGamesparksAPI::registrationRequest( std::string displayName, std::string password, std::string userName, IGameSparks::GSMessageCB callback, void* userData )
	{
        Json::Value request;
		request["displayName"] = displayName;
		request["password"] = password;
		request["userName"] = userName;
        CCGamesparks::getInstance()->sendMessageWithData( ".RegistrationRequest", request, callback, userData );
    }
    
	void CCGamesparksAPI::sendFriendMessageRequest( const std::vector<std::string>& friendIds, std::string message, IGameSparks::GSMessageCB callback, void* userData )
	{
        Json::Value request;
		request["friendIds"] = VecToJsonValue< std::string >( friendIds );
		request["message"] = message;
        CCGamesparks::getInstance()->sendMessageWithData( ".SendFriendMessageRequest", request, callback, userData );
    }
    
	void CCGamesparksAPI::socialLeaderboardDataRequest( std::string challengeInstanceId, int entryCount, const std::vector<std::string>& friendIds, std::string leaderboardShortCode, int offset, bool social, IGameSparks::GSMessageCB callback, void* userData )
	{
        Json::Value request;
		request["challengeInstanceId"] = challengeInstanceId;
		request["entryCount"] = entryCount;
		request["friendIds"] = VecToJsonValue< std::string >( friendIds );
		request["leaderboardShortCode"] = leaderboardShortCode;
		request["offset"] = offset;
		request["social"] = social;
        CCGamesparks::getInstance()->sendMessageWithData( ".SocialLeaderboardDataRequest", request, callback, userData );
    }
    
	void CCGamesparksAPI::twitterConnectRequest( std::string accessSecret, std::string accessToken, IGameSparks::GSMessageCB callback, void* userData )
	{
        Json::Value request;
		request["accessSecret"] = accessSecret;
		request["accessToken"] = accessToken;
        CCGamesparks::getInstance()->sendMessageWithData( ".TwitterConnectRequest", request, callback, userData );
    }
    
	void CCGamesparksAPI::windowsBuyGoodsRequest( std::string currencyCode, std::string receipt, int subUnitPrice, IGameSparks::GSMessageCB callback, void* userData )
	{
        Json::Value request;
		request["currencyCode"] = currencyCode;
		request["receipt"] = receipt;
		request["subUnitPrice"] = subUnitPrice;
        CCGamesparks::getInstance()->sendMessageWithData( ".WindowsBuyGoodsRequest", request, callback, userData );
    }
    
	void CCGamesparksAPI::withdrawChallengeRequest( std::string challengeInstanceId, std::string message, IGameSparks::GSMessageCB callback, void* userData )
	{
        Json::Value request;
		request["challengeInstanceId"] = challengeInstanceId;
		request["message"] = message;
        CCGamesparks::getInstance()->sendMessageWithData( ".WithdrawChallengeRequest", request, callback, userData );
    }
}
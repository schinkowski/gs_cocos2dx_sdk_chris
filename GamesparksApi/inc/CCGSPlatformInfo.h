//
//  CCGSPlatformInfo.h
//  GameSparksTest
//
//  Created by Tomasz Kandziora on 26/04/2014.
//
//

#ifndef CCGSPlatformInfo_h__
#define CCGSPlatformInfo_h__

#include <string>

namespace GameSparks
{
    std::string getPlatformString();
    std::string getDeviceId();
    std::string getDeviceModel();
    std::string getDeviceName();
    std::string getDeviceOS();
    std::string getDeviceType();
}

#endif

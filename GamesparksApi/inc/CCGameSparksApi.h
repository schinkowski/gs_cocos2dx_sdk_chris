//
//  CCGameSparksApi.h
//  GameSparksTest
//
//  Created by Tomasz Kandziora on 25/04/2014.
//
//

#ifndef GameSparksTest_CCGameSparksApi_h
#define GameSparksTest_CCGameSparksApi_h

#include <string>
#include <vector>
#include "IGameSparks.h"

namespace Json {
    class Value;
}

namespace GameSparks
{
    class CCGamesparksAPI
    {
    private:
        const int TIMEOUT;

        CCGamesparksAPI();
        ~CCGamesparksAPI();
        CCGamesparksAPI( const CCGamesparksAPI& rhs );
    public:
        
        static void acceptChallengeRequest( std::string challengeInstanceId, std::string message, IGameSparks::GSMessageCB callback, void* userData );
        static void accountDetailsRequest( IGameSparks::GSMessageCB callback, void* userData );
        static void analyticsRequest( const Json::Value& data, bool end, std::string key, bool start, IGameSparks::GSMessageCB callback, void* userData );
        static void aroundMeLeaderboardRequest( int count, const std::vector<std::string>& friendIds, std::string leaderboardShortCode, bool social, IGameSparks::GSMessageCB callback, void* userData );
        static void authenticationRequest( std::string userName, std::string password, IGameSparks::GSMessageCB callback, void* userData );
        static void buyVirtualGoodsRequest( int currencyType, int quantity, std::string shortCode, IGameSparks::GSMessageCB callback, void* userData );
        static void changeUserDetailsRequest( std::string displayName, IGameSparks::GSMessageCB callback, void* userData );
        static void chatOnChallengeRequest( std::string challengeInstanceId, std::string message, IGameSparks::GSMessageCB callback, void* userData );
        static void consumeVirtualGoodRequest( int quantity, std::string shortCode, IGameSparks::GSMessageCB callback, void* userData );
        static void createChallengeRequest( std::string accessType, std::string challengeMessage, std::string challengeShortCode, int currency1Wager, int currency2Wager, int currency3Wager, int currency4Wager, int currency5Wager, int currency6Wager, int endTime, int expiryTime, int maxAttempts, int maxPlayers, int minPlayers, int startTime, const std::vector<std::string>& usersToChallenge, IGameSparks::GSMessageCB callback, void* userData );
        static void declineChallengeRequest( std::string challengeInstanceId, std::string message, IGameSparks::GSMessageCB callback, void* userData );
        static void deviceAuthenticationRequest( std::string deviceId, std::string deviceModel, std::string deviceName, std::string deviceOS, std::string deviceType, std::string operatingSystem, IGameSparks::GSMessageCB callback, void* userData );
        static void dismissMessageRequest( std::string messageId, IGameSparks::GSMessageCB callback, void* userData );
        static void endSessionRequest( IGameSparks::GSMessageCB callback, void* userData );
        static void facebookConnectRequest( std::string accessToken, std::string code, IGameSparks::GSMessageCB callback, void* userData );
        static void findChallengeRequest( std::string accessType, int count, int offset, IGameSparks::GSMessageCB callback, void* userData );
        static void getChallengeRequest( std::string challengeInstanceId, std::string message, IGameSparks::GSMessageCB callback, void* userData );
        static void getDownloadableRequest( std::string shortCode, IGameSparks::GSMessageCB callback, void* userData );
        static void getMessageRequest( std::string messageId, IGameSparks::GSMessageCB callback, void* userData );
        static void getRunningTotalsRequest( const std::vector<std::string>& friendIds, std::string shortCode, IGameSparks::GSMessageCB callback, void* userData );
        static void getUploadUrlRequest( const Json::Value& uploadData, IGameSparks::GSMessageCB callback, void* userData );
        static void getUploadedRequest( std::string uploadId, IGameSparks::GSMessageCB callback, void* userData );
        static void googlePlayBuyGoodsRequest( std::string currencyCode, std::string signature, std::string signedData, int subUnitPrice, IGameSparks::GSMessageCB callback, void* userData );
        static void iOSBuyGoodsRequest( std::string currencyCode, std::string receipt, bool sandbox, int subUnitPrice, IGameSparks::GSMessageCB callback, void* userData );
        static void joinChallengeRequest( std::string challengeInstanceId, std::string message, IGameSparks::GSMessageCB callback, void* userData );
        static void leaderboardDataRequest( std::string challengeInstanceId, int entryCount, const std::vector<std::string>& friendIds, std::string leaderboardShortCode, int offset, bool social, IGameSparks::GSMessageCB callback, void* userData );
        static void listAchievementsRequest( IGameSparks::GSMessageCB callback, void* userData );
        static void listChallengeRequest( int entryCount, int offset, std::string shortCode, std::string state, IGameSparks::GSMessageCB callback, void* userData );
        static void listChallengeTypeRequest( IGameSparks::GSMessageCB callback, void* userData );
        static void listGameFriendsRequest( IGameSparks::GSMessageCB callback, void* userData );
        static void listInviteFriendsRequest( IGameSparks::GSMessageCB callback, void* userData );
        static void listLeaderboardsRequest( IGameSparks::GSMessageCB callback, void* userData );
        static void listMessageRequest( int entryCount, int offset, IGameSparks::GSMessageCB callback, void* userData );
        static void listMessageSummaryRequest( int entryCount, int offset, IGameSparks::GSMessageCB callback, void* userData );
        static void listVirtualGoodsRequest( IGameSparks::GSMessageCB callback, void* userData );
        static void logChallengeEventRequest( std::string challengeInstanceId, std::string eventKey, IGameSparks::GSMessageCB callback, void* userData );
        static void logEventRequest( std::string eventKey, IGameSparks::GSMessageCB callback, void* userData );
        static void pushRegistrationRequest( std::string deviceOS, std::string pushId, IGameSparks::GSMessageCB callback, void* userData );
        static void registrationRequest( std::string displayName, std::string password, std::string userName, IGameSparks::GSMessageCB callback, void* userData );
        static void sendFriendMessageRequest( const std::vector<std::string>& friendIds, std::string message, IGameSparks::GSMessageCB callback, void* userData );
        static void socialLeaderboardDataRequest( std::string challengeInstanceId, int entryCount, const std::vector<std::string>& friendIds, std::string leaderboardShortCode, int offset, bool social, IGameSparks::GSMessageCB callback, void* userData );
        static void twitterConnectRequest( std::string accessSecret, std::string accessToken, IGameSparks::GSMessageCB callback, void* userData );
        static void windowsBuyGoodsRequest( std::string currencyCode, std::string receipt, int subUnitPrice, IGameSparks::GSMessageCB callback, void* userData );
        static void withdrawChallengeRequest( std::string challengeInstanceId, std::string message, IGameSparks::GSMessageCB callback, void* userData );
    };
}

#endif

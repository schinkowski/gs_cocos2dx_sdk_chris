#ifndef CCGAMESPARKS_H__
#define CCGAMESPARKS_H__

#include <string>
#include "CCGameSparksApi.h"
#include "cocoa/CCObject.h"
#include "IGameSparks.h"

namespace Json
{
    class Value;
}

namespace GameSparks
{
    class IGameSparks;
    
    struct ConnectionMode
    {
        enum Enum
        {
            keDEBUG,
            keLIVE
        };
    };
    
    class CCGamesparks : public cocos2d::CCObject
    {
    private:
        const unsigned int REQUEST_TIMEOUT;
        double interval;
        double unitsPerSecond;
        
        IGameSparks *gsClient;
        
        static CCGamesparks* instance;
        
        void update( float dt );
        
        CCGamesparks();
        virtual ~CCGamesparks();
        CCGamesparks( const CCGamesparks& rhs );
    public:
        static CCGamesparks* create();
        static CCGamesparks* getInstance();
        static void destroyInstance();
        
        void connect( std::string apiKey, std::string apiSecret, ConnectionMode::Enum mode = ConnectionMode::keDEBUG );
        
        //Has to be provided to specify what format the delta time is in update function.
        //For instance:
        //  1s == 1s
        //  1000ms == 1s
        void setUnitsPerSecond( double _units );
        
        //Amount of time at which the Gamesparks client will update it self. Default at once per second.
        void setUpdateInterval( double _interval );
        
        void setLogCallback ( IGameSparks::GSLogCallback cb, void *userData);
        
        void suspend();
        void resume();
        
        bool isAuthenticated();
        
        void setOnError ( IGameSparks::GSMessageCB callback, void *data );
        void setOnMessage ( IGameSparks::GSMessageCB callback, void *data );
        void setOnConnect ( IGameSparks::GSConnectionCB callback, void *data );
        void setOnDisconnect ( IGameSparks::GSConnectionCB callback, void *data );
        
        void sendMessageWithData(   std::string message,
                                    Json::Value& root,
                                    IGameSparks::GSMessageCB callback = 0,
                                    void *userData = 0
                                 );
    };
}

#endif //CCGAMESPARKS_H__
package com.gamesparks.example;

import org.cocos2dx.lib.Cocos2dxActivity;

import android.os.Bundle;
import android.provider.Settings.Secure;

public class GameSparks
{
    public static String getDeviceId()
    {
        return Secure.getString( Cocos2dxActivity.getContext().getContentResolver(), Secure.ANDROID_ID );
    }

    public static String getDeviceModel()
    {
        return "";
    }

    public static String getDeviceName()
    {
        return "";
    }

    public static String getDeviceOS()
    {
        return "";
    }

    public static String getDeviceType()
    {
        return "";
    }

}